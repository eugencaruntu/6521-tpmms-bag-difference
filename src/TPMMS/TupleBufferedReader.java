/*
 * COMP 6521, Project 1
 * Annabelle Williams           40038085
 * Eugen Caruntu                29077103
 * Ernesto Garsiya Melikhov     40039957
 * Juyoun Kim                   40016972
 */

package TPMMS;

import java.io.*;

/**
 * A custom buffer extending BufferedReader storing the first line in a tuple attribute
 * which allows for peek() that is required when comparing multiple buffers
 * Also provides isEmpty() and pop() refresh functionality
 */
class TupleBufferedReader extends BufferedReader {
    private static int bufferSize;
    private int lineReads;
    private String firstTuple;
    private boolean isEmpty;
    
    /**
     * Constructor delegating to super and setting the attributes through scanFirstLine()
     *
     * @param file       the file under the buffer
     * @param bufferSize the size of the buffer
     *
     * @throws FileNotFoundException
     */
    public TupleBufferedReader(File file, int bufferSize) throws FileNotFoundException {
        super(new FileReader(file), bufferSize);
        TupleBufferedReader.bufferSize = bufferSize;
        scanFirstLine();
    }
    
    /**
     * Sets the tuple and isEmpty attributes by reading the first line in the buffer
     * Must be called in each method that reads line (removes first buffer tuple)
     */
    private void scanFirstLine() {
        try {
            firstTuple = this.readLine();
            lineReads++;
            if (firstTuple == null) {
                isEmpty = true;
            } else {
                isEmpty = false;
            }
        } catch (IOException e) {
            isEmpty = true;
            firstTuple = null;
            System.out.println(e.getMessage());
        }
    }
    
    /**
     * Gets the number of disk reads
     *
     * @return the number of disk reads
     */
    public int getIOReads() {
        return (int) Math.ceil(lineReads * 102.4 / bufferSize);
    }
    
    /**
     * Get the value of the first line then replace the tuple attribute with the next one
     *
     * @return the initial tuple value
     */
    public String pop() {
        // get the tuple attribute value
        String tuple = peek();
        // read a new line and set it as the tuple attribute value
        scanFirstLine();
        // return the previous value of the tuple
        return tuple;
    }
    
    /**
     * Looks at the top of the buffer without "removing" the tuple
     *
     * @return null if buffer is empty, the first tuple otherwise
     */
    public String peek() {
        return (isEmpty) ? null : firstTuple;
    }
    
    /**
     * Returns the isEmpty() attribute of the buffer
     *
     * @return true if buffer is empty, false otherwise
     */
    public boolean isEmpty() {
        return isEmpty;
    }
    
}