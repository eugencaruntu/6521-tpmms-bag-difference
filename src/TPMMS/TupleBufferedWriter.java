/*
 * COMP 6521, Project 1
 * Annabelle Williams           40038085
 * Eugen Caruntu                29077103
 * Ernesto Garsiya Melikhov     40039957
 * Juyoun Kim                   40016972
 */

package TPMMS;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class TupleBufferedWriter extends BufferedWriter {
    private int lineWrites;
    private int bufferSize;
    
    public TupleBufferedWriter(File file, int bufferSize) throws IOException {
        super(new FileWriter(file), bufferSize);
        this.bufferSize = bufferSize;
    }
    
    /**
     * Gets the number of disk reads
     *
     * @return the number of disk reads
     */
    public int getIOWrites() {
        return (int) Math.ceil(lineWrites * 102.4 / bufferSize);
    }
    
    /**
     * Calls the write() method in BufferedWriter with str parameter, and increments lineWrite counter if countLine
     * boolean value is true
     *
     * @param str String value to write to the buffer
     *            //     * @param countLines    Boolean value to decide whether or not to increment the lineWrite counter
     *
     * @throws IOException
     */
    public void write(String str) throws IOException {
        super.write(str);
        lineWrites++;
    }
}
