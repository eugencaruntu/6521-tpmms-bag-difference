/*
 * COMP 6521, Project 1
 * Annabelle Williams           40038085
 * Eugen Caruntu                29077103
 * Ernesto Garsiya Melikhov     40039957
 * Juyoun Kim                   40016972
 */

package TPMMS;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Run class wrapping an ArrayList of String
 * Provides writing to file
 */
public class Run extends ArrayList<String> {
    
    /**
     * default ctor
     */
    public Run(int initialSize) {
        super(initialSize);
    }
    
    
    /**
     * Writing the sub-list to file into a designated location)
     * Optimization possible: the lists can be serialized in txt files on disk but they must later be loaded in memory in full (thus their size might not be the desired one)
     *
     * @param bufferSize     the size of the output buffer
     * @param outputFileName the folder destination to write the run on disk
     */
    public int toDisk(String outputFileName, int bufferSize) {
        int ioWrites = 0;
        try (TupleBufferedWriter bufferedWriter = new TupleBufferedWriter(new File(outputFileName), bufferSize)) {
            for (String tuple : this) {
                bufferedWriter.write(tuple);
                bufferedWriter.newLine();
            }
            ioWrites = bufferedWriter.getIOWrites();
            bufferedWriter.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return ioWrites;
    }
    
}
