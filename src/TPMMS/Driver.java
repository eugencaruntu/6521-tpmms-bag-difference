/*
 * COMP 6521, Project 1
 * Annabelle Williams           40038085
 * Eugen Caruntu                29077103
 * Ernesto Garsiya Melikhov     40039957
 * Juyoun Kim                   40016972
 */

package TPMMS;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.PriorityQueue;

public class Driver {
    private final static int blockSize = 4 * 1024;       // 4KB
    private static int ioReads, ioWrites;
    
    public static void main(String[] args) {
        final File inputFolder = new File("demo/input/");
        final File tempFolder = new File("demo/temp/");
        final File sortedFolder = new File("demo/sorted/");
        final File relation1 = new File("demo/sorted/bag1_sorted.txt");
        final File relation2 = new File("demo/sorted/bag2_sorted.txt");
        int blockMemoryFactor = (Runtime.getRuntime().maxMemory() < 6000000) ? 1 : 2;   // sets an coefficient for bufferSize
        setOutputLocation(tempFolder, sortedFolder);
        long startTime = System.nanoTime();
        phaseOne(inputFolder, tempFolder, 45 * blockSize);
        System.out.printf("%n%13s%10s%10s%12s%10s%n", "", "Reads", "Writes", "Total IO", "Time");
        System.out.printf("after phase 1%10d%10d%12d%10.2f%n", ioReads, ioWrites, (ioReads + ioWrites), ((double) (System.nanoTime() - startTime)) / 1000000000);
        phaseTwo(tempFolder, sortedFolder, blockMemoryFactor * 5 * blockSize);
        System.out.printf("after phase 2%10d%10d%12d%10.2f%n", ioReads, ioWrites, (ioReads + ioWrites), ((double) (System.nanoTime() - startTime)) / 1000000000);
        String result = bagDifference(relation1, relation2, blockMemoryFactor * 100 * blockSize);
        System.out.printf("after bag diff.%8d%10d%12d%10.2f%n", ioReads, ioWrites, (ioReads + ioWrites), ((double) (System.nanoTime() - startTime)) / 1000000000);
        System.out.println(result);
    }
    
    /**
     * Cleans up and sets the output location
     */
    public static void setOutputLocation(File tempFolder, File sortedFolder) {
        // clean folders
        cleanFolders(tempFolder);
        cleanFolders(sortedFolder);
        // make folders if missing
        tempFolder.mkdir();
        sortedFolder.mkdir();
    }
    
    /**
     * Phase 1
     * Make lists (runs) from tuple buffer as large as the available memory
     * Sort the run in memory
     * Write them to disk in temporary location
     */
    public static void phaseOne(File inputFolder, File tempFolder, int bufferSize) {
        
        // process each file in input folder
        for (File file : inputFolder.listFiles()) {
            File folder = tempFolder(file, tempFolder);
            int subListSize = (int) ((getAvailableMemory() * 0.3) / (blockSize / 40));
            
            // a temporary run to be dumped on disk once its size fills the memory
            Run run = new Run(subListSize);
            String tuple;
            // make a tuple buffered reader
            try (TupleBufferedReader tupleBufferedReader = new TupleBufferedReader(file, bufferSize)) {
                // as long as there is content in file, fill memory and dump to disk
                do {
                    for (int i = 0; i < subListSize; i++) {
                        tuple = tupleBufferedReader.pop();
                        if (tuple == null) {
                            break;
                        } else {
                            run.add(tuple);
                        }
                    }
                    
                    // sort the run in memory
                    Collections.sort(run);
                    
                    // write the sorted run to disk
                    ioWrites += run.toDisk(folder + "/" + "phase_1_" + System.currentTimeMillis() + ".txt", bufferSize);
                    run.clear();
                } while (!tupleBufferedReader.isEmpty());
                
                // close the tuple buffer
                ioReads += tupleBufferedReader.getIOReads();
                tupleBufferedReader.close();
                
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println(file.getName() + " was split into " + folder.listFiles().length + " sub-lists, having " + subListSize + " tuples each");
        }
    }
    
    /**
     * Phase 2
     * Merge and sort for each file in temporary folder
     */
    public static void phaseTwo(File tempFolder, File sortedFolder, int bufferSize) {
        
        // for each temporary subfolder of the output folder
        for (File folder : tempFolder.listFiles(File::isDirectory)) {
            mergeAndSort(folder, sortedFolder, bufferSize);
            System.gc();
        }
    }
    
    /**
     * Make tuple buffered readers for each file in specified temporary folder
     * Collect these buffered readers into a priority queue having the smallest tuple as head
     * Remove the smallest tuple from head and write it to the output buffer, then refresh the buffer that provided the smallest tuple
     * Continue until the queue is empty
     *
     * @param tempFolder   the folder with runs already sorted (sub-lists)
     * @param sortedFolder the output for the sorted relation
     * @param bufferSize   the size of a block to be read and write at once
     */
    public static void mergeAndSort(File tempFolder, File sortedFolder, int bufferSize) {
        
        // make a priority queue of custom tuple buffers, comparing the first line from each using custom peek feature
        PriorityQueue<TupleBufferedReader> tupleQueue = new PriorityQueue<>
                (tempFolder.listFiles().length, Comparator.comparing(TupleBufferedReader::peek));
        
        try (TupleBufferedWriter bufferedWriter = new TupleBufferedWriter(new File(sortedFolder + "/" + tempFolder.getName() + "_sorted.txt"), bufferSize)) {
            // as long as there are files to merge, make a buffer and add to the priority queue
            for (File file : tempFolder.listFiles(File::isFile)) {
                TupleBufferedReader tupleBufferedReader = new TupleBufferedReader(file, bufferSize);
                tupleQueue.add(tupleBufferedReader);
            }
            String tuple;
            TupleBufferedReader tupleBufferedReader;
            do {
                // remove the buffer having the smallest tuple at the top of the priority queue
                tupleBufferedReader = tupleQueue.poll();
                // pop the smallest value from the buffer
                tuple = tupleBufferedReader.pop();
                // write to file
                bufferedWriter.write(tuple);
                bufferedWriter.newLine();
                // close the buffer if is empty
                if (tupleBufferedReader.isEmpty()) {
                    ioReads += tupleBufferedReader.getIOReads();
                    tupleBufferedReader.close();
                } else { // reload the buffer from which we popped if has more tuples
                    tupleQueue.add(tupleBufferedReader);
                }
            } while (tupleQueue.size() > 0);
            ioWrites += bufferedWriter.getIOWrites();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Obtain the available memory
     *
     * @return the available memory
     *
     * @link https://stackoverflow.com/questions/12807797/java-get-available-memory
     */
    public static long getAvailableMemory() {
        System.gc();
        // How much memory is already in use (allocated)
        long allocatedMemory = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        
        // from the max memory set in VM, substract the allocated memory
        return Runtime.getRuntime().maxMemory() - allocatedMemory;
    }
    
    /**
     * Makes a subfolder (within the output folder) for temporary files using the name of the initial relation
     *
     * @param file         the initial relation
     * @param outputFolder the output folder
     *
     * @return the subfolder for temporary files
     */
    public static File tempFolder(File file, File outputFolder) {
        File tempFolder = new File(outputFolder + "/" + file.getName().substring(0, file.getName().lastIndexOf(".")));
        if (!tempFolder.exists()) {
            tempFolder.mkdir();
        }
        return tempFolder;
    }
    
    /**
     * Recursively clean a folder and its subfolders and files
     *
     * @param folder the parent folder to be cleaned
     */
    public static void cleanFolders(File folder) {
        if (folder.exists()) {
            for (File file : folder.listFiles()) {
                if (file.isDirectory()) {
                    cleanFolders(file);
                }
                file.delete();
            }
        }
    }
    
    /**
     * Bag Difference
     * A counter is maintained while comparing the head from each tuple buffer
     * Each tuple and result is written on a file
     */
    public static String bagDifference(File t1, File t2, int bufferSize) {
        int lineCount = 0;
        
        try (
                // make a buffer for each one
                TupleBufferedReader b1 = new TupleBufferedReader(t1, bufferSize);
                TupleBufferedReader b2 = new TupleBufferedReader(t2, bufferSize);
                // make an output buffer
                TupleBufferedWriter bufferedWriter = new TupleBufferedWriter(new File(t1.getAbsolutePath().substring(0, t1.getAbsolutePath().lastIndexOf(".")) + "_bag_difference.txt"), bufferSize)) {
            // read line from T1 and count downwards how many of same ID exist
            int t1counter, t2counter = 0;
            String t1Line;
            String t1NextLine;
            t1NextLine = b1.pop();
            String t2Line = b2.pop();
            
            // FIRST RELATION
            while ((t1Line = t1NextLine) != null) {
                t1counter = 1;
                
                // read and count the t1 until next one is a different tuple
                while ((t1NextLine = b1.pop()) != null && t1Line.equals(t1NextLine)) {
                    t1counter++;            // increment each time the lines are equal
                }   // we are out and next line is the next different value, will be assigned to t1Line once we go again in the parent loop
                
                // SECOND RELATION
                
                // read line from T2 and compare with T1, if matches, count how many exist in T2
                while (t2Line != null) {
                    
                    // break if we already have bigger values in second relation (should not happen until the end eventually)
                    if (t2Line.compareTo(t1Line) > 0) {
                        break;
                    }
                    
                    // bypass lower values
                    while (t2Line != null && t2Line.compareTo(t1Line) < 0) {
                        t2Line = b2.pop();  // pop if values are less
                    } // when exiting this loop the second id is matching the first id
                    
                    // from here we should have equal values...
                    while (t2Line != null && t2Line.equals(t1Line)) {
                        t2counter++;
                        t2Line = b2.pop();
                    }
                    
                    // substract from count of T1
                    t1counter -= t2counter;
                    t2counter = 0;
                    break;
                }
                
                if (t1counter >= 0) {
                    // write the T1 line and the count to file
                    bufferedWriter.write(t1Line + "\t" + t1counter);
                    bufferedWriter.newLine();
                    lineCount++;
                }
                // next T1 line
            }
            ioReads += b1.getIOReads() + b2.getIOReads();
            ioWrites += bufferedWriter.getIOWrites();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "\nThe bag difference has " + lineCount + " tuples, and uses " + (int) Math.ceil(lineCount / 40) + " blocks on disk";
    }
}

