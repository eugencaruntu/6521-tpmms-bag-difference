# COMP-6521_Project1
Project 1 implemented as part of the COMP 6521 course (Winter 2018 term)

  * Annabelle Williams           40038085
  * Eugen Caruntu                29077103
  * Ernesto Garsiya Melikhov     40039957
  * Juyoun Kim                   40016972

###Delivery notes:
The Two Pass Multi-way Merge Sort algorithm is implemented fully (after phase 1 and 2 the sorted relations are saved on disk).
Bag difference is using the sorted relations from disk and saves the result to file.

The application is optimized for sample relations (each one of the two holds roughly 1.2 M tuples)

Tests were performed with 5MB and 10MB maximum memory and the execution time was considered secondary as importance to IO operations.

Adjustments can be done so that the solution performs faster with more IO activity.

Buffers size impacts the IO operations but also can reduce the memory space.

All IO are reported, including for saving the bag difference result to disk.
